# require scipy v0.15
from scipy.optimize import linprog
import numpy as np

#Minimize:   c^T * x
#Subject to: A_ub * x <= b_ub
#            A_eq * x == b_eq

c = np.array([1] * 5) # weights
A_ub = np.array([
	[1, 1, 0, 0, 0],
	[0, 1, 1, 0, 0],
	[0, 0, 1, 1, 0],
	[0, 0, 0, 1, 1],
	[-1, 0, 0, 0, 0],
	[0, -1, 0, 0, 0],
	[0, 0, -1, 0, 0],
	[0, 0, 0, -1, 0],
	[0, 0, 0, 0, -1]
])
b_ub = np.array([4, 4, 4, 4, 0, 0, 0, 0, 0])


res = linprog(c=c, A_ub=A_ub, b_ub=b_ub)
print res